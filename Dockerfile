FROM debian:bullseye-slim AS cpputest_builder
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install apt-utils &&\
    apt -yq install build-essential cmake git autoconf libtool

RUN cd /root/ && git clone git://github.com/cpputest/cpputest.git && \
    cd cpputest/cpputest_build && autoreconf .. -i && ../configure && \
    make


FROM alpine:latest
RUN apk --no-cache add bash gcc g++ newlib-arm-none-eabi gcc-arm-none-eabi make cmake gcovr
WORKDIR /root/
COPY --from=cpputest_builder /root/cpputest ./cpputest
RUN cd /root/cpputest/cpputest_build && make install
